package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.Batoh;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.io.InputStream;
import java.util.Set;

public class PanelBatohu implements Component, Observer {
    private final Batoh batoh;
    private VBox vBox = new VBox();
    private final FlowPane panelVeciFlowPane = new FlowPane();

    public PanelBatohu(IHra hra)  {
        this.batoh = hra.getBatoh();

        init();

        batoh.registerObserver(this);
    }

    private void init() {
        vBox.setPrefWidth(100.0);
        Label label = new Label("Věci v batohu:");
        vBox.getChildren().addAll(label, panelVeciFlowPane);
    }

    @Override
    public Node getComponentNode() {
        return this.vBox;
    }

    @Override
    public void update() {
        nactiObrazkyVeci();
    }

    private void nactiObrazkyVeci() {
        panelVeciFlowPane.getChildren().clear();
        String nazevObrazku;
        Set<String> mnozinaVeci = batoh.getMnozinaVeci();
        for (String nazevVeci : mnozinaVeci) {
            nazevObrazku = "/zdroje/" + nazevVeci + ".jpg";
            InputStream inputStream = PanelBatohu.class.getResourceAsStream(nazevObrazku);
            Image image = new Image(inputStream, 100, 100, false, false);
            ImageView imageView = new ImageView(image);
            panelVeciFlowPane.getChildren().add(imageView);
        }
    }
}
