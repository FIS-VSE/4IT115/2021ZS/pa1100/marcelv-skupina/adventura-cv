package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListView;

public class PanelVychodu implements Observer, Component {

    HerniPlan herniPlan;
    ListView<String> listView = new ListView<>();
    ObservableList<String> vychody = FXCollections.observableArrayList();

    public PanelVychodu(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;

        init();

        herniPlan.registerObserver(this);
    }

    private void init() {
        nactiAktualniSeznamVychodu();

        listView.setItems(vychody);
        listView.setPrefWidth(100.0);

    }

    @Override
    public void update() {
        nactiAktualniSeznamVychodu();
    }

    private void nactiAktualniSeznamVychodu() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        this.vychody.clear();
        for (Prostor prostor : aktualniProstor.getVychody()) {
            vychody.add(prostor.getNazev());
        }
    }

    @Override
    public Node getComponentNode() {
        return listView;
    }
}
