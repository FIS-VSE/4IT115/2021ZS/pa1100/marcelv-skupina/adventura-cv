package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.MapaHry;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdventuraZaklad extends Application {

    private ImageView viewKarkulky = new ImageView(
            new Image(MapaHry.class.getResourceAsStream("/zdroje/karkulka.png"),
                    30.0, 40.0, false, true));
    private ImageView viewVlka = new ImageView(
            new Image(MapaHry.class.getResourceAsStream("/zdroje/vlk.png"),
                    80.0, 60.0, false, true));
    private final Image srdceImage = new Image(MapaHry.class.getResourceAsStream("/zdroje/srdce.png"),
            30.0, 40.0, false, true);
    private ImageView viewSrdce1 = new ImageView(
            srdceImage);

    public static void main(String[] args) {
        if (args.length > 0) {
            String param = args[0];
            if (param.equals("text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
            } else if (param.equals("graphic")) {
                launch(args);
            } else {
                throw new IllegalArgumentException("Argument musí být text nebo grafik. Byl zadán: " + param);
            }
        } else {
            launch(args);
        }
    }

    @Override
    public void start(Stage primaryStage) {
        IHra hra = new Hra();
        BorderPane borderPane = new BorderPane();

        TextArea konzole = vytvorKonzoli(hra, borderPane);
        Label prikazovePoleLabel = new Label("Zadej příkaz: ");
        prikazovePoleLabel.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 16.0));
        TextField prikazovePole = vytvorPrikazovePole(konzole, hra);
        HBox spodniPanel = vytvorSpodniPanel(prikazovePoleLabel, prikazovePole);
        spodniPanel.setAlignment(Pos.CENTER);

        borderPane.setBottom(spodniPanel);

        PanelVychodu panelVychodu = new PanelVychodu(hra.getHerniPlan());
        borderPane.setRight(panelVychodu.getComponentNode());

        MapaHry mapaHry = new MapaHry(hra.getHerniPlan(), viewKarkulky);
        AnchorPane mapaHryAnchorPane = mapaHry.getComponentNode();
        borderPane.setTop(mapaHryAnchorPane);

        pridejSrdceAVlka(mapaHryAnchorPane);

        PanelBatohu panelBatohu = new PanelBatohu(hra);
        borderPane.setLeft(panelBatohu.getComponentNode());

        Scene scene = new Scene(borderPane, 600.0, 500.0);
        animujPohyb(scene);
        primaryStage.setTitle("Adventura Cviceni");
        primaryStage.setScene(scene);
        prikazovePole.requestFocus();
        primaryStage.show();
    }

    private void animujPohyb(Scene scene) {
        double karkulcinaKonstanta = 300.0; // pixels per second

        LongProperty lastUpdateTime = new SimpleLongProperty();
        DoubleProperty karkulkyPohybX = new SimpleDoubleProperty(10.0);
        DoubleProperty karkulkyPohybY = new SimpleDoubleProperty(10.0);

        AnimationTimer karkulkaAnimation = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (lastUpdateTime.get() > 0) {
                    double elapsed = (timestamp - lastUpdateTime.get())
                            / 1_000_000_000.0 ;

                    double oldX = viewKarkulky.getTranslateX();
                    double zmenaX = elapsed * karkulkyPohybX.get();
                    double oldY = viewKarkulky.getTranslateY();
                    double zmenaY = elapsed * karkulkyPohybY.get();
                    viewKarkulky.setTranslateX(oldX + zmenaX);
                    viewKarkulky.setTranslateY(oldY + zmenaY);
                }
                lastUpdateTime.set(timestamp);
            }
        };
        karkulkaAnimation.start();

        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.A) {
                karkulkyPohybX.set(-karkulcinaKonstanta);
            }
            if (event.getCode() == KeyCode.D) {
                karkulkyPohybX.set(karkulcinaKonstanta);
            }
            if (event.getCode() == KeyCode.W) {
                karkulkyPohybY.set(-karkulcinaKonstanta);
            }
            if (event.getCode() == KeyCode.S) {
                karkulkyPohybY.set(karkulcinaKonstanta);
            }
        });

        scene.setOnKeyReleased(e -> {
            switch (e.getCode()) {
                case A:
                case D:
                    karkulkyPohybX.set(0);
                    break;
                case W:
                case S:
                    karkulkyPohybY.set(0);
                    break;
            }
        });
    }

    private void pridejSrdceAVlka(AnchorPane mapaHryAnchorPane) {
        mapaHryAnchorPane.getChildren().addAll(viewSrdce1, viewVlka);

        viewVlka.setBlendMode(BlendMode.COLOR_BURN);

        AnchorPane.setLeftAnchor(viewVlka, 250.0);
        AnchorPane.setTopAnchor(viewVlka, 50.0);

        AnchorPane.setLeftAnchor(viewSrdce1, 300.0);
        AnchorPane.setTopAnchor(viewSrdce1, 250.0);
    }

    private TextArea vytvorKonzoli(IHra hra, BorderPane borderPane) {
        TextArea konzole = new TextArea();
        konzole.setText(hra.vratUvitani());
        borderPane.setCenter(konzole);
        konzole.setEditable(false);
        return konzole;
    }

    private TextField vytvorPrikazovePole(TextArea textArea, IHra hra) {
        TextField prikazovePole = new TextField();
        prikazovePole.setOnAction(event -> {
            String prikaz = prikazovePole.getText();
            prikazovePole.setText("");
            String navratovaHodnotaHry = hra.zpracujPrikaz(prikaz);
            if (hra.konecHry()) {
                prikazovePole.setEditable(false);
            }
            textArea.appendText("\n" + navratovaHodnotaHry + "\n");
        });
        return prikazovePole;
    }

    private HBox vytvorSpodniPanel(Label prikazovePoleLabel, TextField prikazovePole) {
        HBox spodniPanel = new HBox();
        spodniPanel.getChildren().add(prikazovePoleLabel);
        spodniPanel.getChildren().add(prikazovePole);
        return spodniPanel;
    }
}
