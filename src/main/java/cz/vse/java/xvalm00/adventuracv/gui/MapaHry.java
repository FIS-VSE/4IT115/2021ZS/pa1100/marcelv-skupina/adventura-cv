package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class MapaHry implements Observer, Component {
    private AnchorPane anchorPane = new AnchorPane();

    private final HerniPlan herniPlan;
    private ImageView viewKarkulky;

    public MapaHry(HerniPlan herniPlan, ImageView viewKarkulky) {
        this.herniPlan = herniPlan;
        this.viewKarkulky = viewKarkulky;

        init();

        herniPlan.registerObserver(this);
    }

    private void init() {
        ImageView imageView = new ImageView(new Image(MapaHry.class.getResourceAsStream("/zdroje/herniPlan.png"), 400.0, 250.0, false, true));
        anchorPane.getChildren().add(imageView);
        anchorPane.getChildren().add(viewKarkulky);
        vykresliTecku();
    }

    @Override
    public void update() {
        vykresliTecku();
    }

    private void vykresliTecku() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        AnchorPane.setLeftAnchor(viewKarkulky, aktualniProstor.getPosLeft());
        AnchorPane.setTopAnchor(viewKarkulky, aktualniProstor.getPosTop());
    }

    @Override
    public AnchorPane getComponentNode() {
        return anchorPane;
    }
}
