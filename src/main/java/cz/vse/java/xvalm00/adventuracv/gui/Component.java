package cz.vse.java.xvalm00.adventuracv.gui;

import javafx.scene.Node;

public interface Component {

    Node getComponentNode();

}
